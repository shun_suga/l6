class PostsController < ApplicationController
  def index
    @posts = Post.all
  end

  def new
    @post = Post.new
  end

  def create
    @post = current_user.posts.new(post_params)

    if @post.save
      redirect_to posts_path, notice: "投稿「#{@post.comment}」を保存しました"
    else
      render :new
    end
  end

  def destroy
    set_post
    @post.destroy
    redirect_to posts_path, notice: "投稿「#{@post.comment}」を削除しました"
  end

  def show
    set_post
  end

  def edit
    set_post
  end

  def update
    @post = set_post

    if @post.update(post_params)
      redirect_to @post, notice: "投稿を「#{@post.comment}」更新しました"
    else
      render :edit, notice: "投稿の編集を失敗しました。"
    end
  end

  private
  def set_post
    @post = Post.find(params[:id])
  end
  def post_params
    params.require(:post).permit(:comment)
  end
end
