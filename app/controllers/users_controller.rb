class UsersController < ApplicationController
  def index
    @users = User.all
  end

  def show
    set_user
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to users_path, notice: "ユーザー「#{@user.uid}」を登録しました。"
    else
      render :new
    end
  end

  def edit
    set_user
  end

  def update
    @user = User.new(user_params)

    if @user.save
      redirect_to user_path, notice: "ユーザー「#{@user.uid}」更新しました。"
    else
      render :edit
    end
  end

  def destroy
    set_user
    @user.destroy
    redirect_to users_path, notice: "ユーザー「#{@user.uid}」を削除しました。"
  end

  private
  def user_params
    params.require(:user).permit(:uid, :password, :password_confirmation)
  end
  def set_user
    @user = User.find(params[:id])
  end
end
